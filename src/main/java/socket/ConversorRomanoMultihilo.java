package socket;

import java.net.*;
import java.io.*;
import romano.ConversorRomano;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomanoMultihilo {

    public static void main(String[] args) {
        ServerSocket servidor = null;
        Socket cliente;
        
        try {
            //Abre un servidor socket local en el puerto 6400
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
        	System.out.println("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        LogUtil.INFO("Socket iniciado, se atienden peticiones..");
        System.out.println("Socket iniciado, se atienden peticiones..");
        //TODO: Pendiente de implementacion del procesador de peticiones
        try {
        					while (true) {
        					// el servidor socket queda esperando las conexiones
        					
        						cliente = servidor.accept();
        				ConversorRomanoThread c = new ConversorRomanoThread(cliente, servidor);
        					c.start();
        					}
        				} catch (IOException ie) {
        				  System.out.println("Error al procesar socket. " + ie.getMessage());
        			}
        	     
        	
    }

}