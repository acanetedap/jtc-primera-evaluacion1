package socket;

import java.net.Socket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import datos.LogConversiones;
import datos.LogConversionesManager;
import romano.ConversorRomano;

import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    private ServerSocket servidor;
    
    public ConversorRomanoThread(Socket cliente, ServerSocket servidor) {
        this.cliente = cliente;
        this.servidor = servidor;
    }

    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida..." + this.getName());
    	    	System.out.println("Nueva peticion recibida..." + this.getName());

        //TODO: Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
    	    	try {
      				OutputStream clientOut = cliente.getOutputStream();
    				PrintWriter pw = new PrintWriter(clientOut, true);

    				// obtenemos el InputStream del cliente
    				InputStream clientIn = cliente.getInputStream();
    				BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

    				
    				String mensajeRecibido = br.readLine();
    				System.out.println("mensajeRecibido = " + mensajeRecibido + " - " + this.getName());

    				
    				if (mensajeRecibido != null) {
    					if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
    						pw.println("Finalizando servicio.");
    						System.out.println("Finalizando servicio.");
    						servidor.close();
    						System.exit(0);
    					} else if (mensajeRecibido.trim().startsWith("conv")) {
    						
    						String n = mensajeRecibido.substring(4);
    						int nro = Integer.parseInt(n.trim());
    						try {
    							pw.println("nro = " + n);
    							String valor = ConversorRomano.decimal_a_romano(nro);
    							pw.println("Valor convertido = " + valor);
    							System.out.println("Valor convertido = " + valor);
    							
    							LogConversiones lc = new LogConversiones(this.getName(),cliente.getInetAddress().getHostAddress(),nro,valor);					
    							LogConversionesManager m = new LogConversionesManager();
    							m.insertarNuevoRegistro(lc);
    							
    						} catch (Exception e) {
    							pw.println(e.getMessage());
    						}
    					}
    				} else {
    					pw.println("Mensaje recibido no válido");
    					System.out.println("Mensaje recibido no válido");
    				}

    				// cerramos conexion con el cliente 
    				clientIn.close();
    				clientOut.close();
    				cliente.close();
    			} catch (IOException ie) {
    			
    				System.out.println("Error al procesar socket. " + ie.getMessage());
    			}
    				
        	LogUtil.INFO("Finalizando atencion de la peticion recibida... " + this.getName());
        	System.out.println("Finalizando atencion de la peticion recibida... " + this.getName());
        	

        }     
    }

